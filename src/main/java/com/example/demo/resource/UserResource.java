package com.example.demo.resource;

import com.example.demo.domain.User;
import com.example.demo.service.UserService;
import org.redisson.Redisson;
import org.redisson.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.RedisZSetCommands;
import org.springframework.data.redis.core.BoundZSetOperations;
import org.springframework.data.redis.core.DefaultTypedTuple;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;


//test Resource
@RestController
@RequestMapping("/user")
public class UserResource {

    private UserService userService;
    private final StringRedisTemplate stringRedisTemplate;
    private Logger logger = LoggerFactory.getLogger(UserResource.class);

    public UserResource(UserService userService, StringRedisTemplate stringRedisTemplate) {
        this.userService = userService;
        this.stringRedisTemplate = stringRedisTemplate;
    }

    @GetMapping("/all")
    public List<User> getAllUserResource() {
        return userService.getAllUsers();
    }

    @GetMapping("/findById/{id}")
    public User findByIdResource(@PathVariable Integer id) {
        return userService.findById(id);
    }

    @GetMapping("/cleanOne/{id}")
    public String cleanCache(@PathVariable Integer id) {
        userService.clear(id);
        return "刪除ID為:" + id + "成功";
    }

    @GetMapping("/cleanAll")
    public String cleanAllCache() {
        userService.clearAllUserCache();
        return "刪除全部成員成功";
    }


    //有序集合zset 有序性靠在數據結構增加屬性score  Spring提供街口TypeTuple   實作是DefaultTypedTuple
    @RequestMapping("/zset")
    public Map<String, Object> testZset() {
        Set<ZSetOperations.TypedTuple<String>> typedTuplesSet = new HashSet<>();

        for (int i = 1; i <= 9; i++) {
            //分數
            double score = i * 0.1; //這邊只是先預設 之後比如說是電傷商銷售排行等 score
            // 創建一個TypedTuple對象 存入值和分數
            // value代表名稱 score代表個別分數
            ZSetOperations.TypedTuple<String> typedTuple = new DefaultTypedTuple("value" + i, score);
            logger.info("ZSerOperations 集合:{}", typedTuple);
            //typedTuplesSet 存入各個ZSetOperations.TypedTuple
            typedTuplesSet.add(typedTuple);
        }
        //往Redis 處理 有序集合插入元素 該分類的key zset1
        stringRedisTemplate.opsForZSet().add("zset1", typedTuplesSet);
        //綁定zset1有序集合操作
        BoundZSetOperations<String, String> zsetOps
                = stringRedisTemplate.boundZSetOps("zset1");

        //利用綁定的 zset1 --zsetOps 增加一個元素--test成功
        zsetOps.add("value10", 0.26);
        Set<String> setRange = zsetOps.range(1, 6);
        logger.info("setRange:{}", setRange);
        //按分數排序獲取有序集合
        Set<String> setScore = zsetOps.rangeByScore(0.2, 0.6);
        logger.info("按分數排序獲取有序集合:{}", setScore);
        //定義值範圍
        RedisZSetCommands.Range range = new RedisZSetCommands.Range();
        range.gt("value3");// 大於value3
//		range.gte("value3");//大於等於value3
//		range.lt("value8"); //小於value8
//		range.lte("value8");//小於等於value8
        logger.info("取範圍大於value3:{}", range);

        //按值排序，請注意這個排序是按字符串排序
        Set<String> setLex = zsetOps.rangeByLex(range);
        logger.info("按值排序，請注意這個排序是按字符串排序:{}", setLex);

        //刪除元素
        zsetOps.remove("value9", "value2");
        logger.info("查看剩餘元素:{}", zsetOps);
        // 求分數
        Double score = zsetOps.score("value8");
        logger.info("value8 分數:{}", score);

        // 在下標區間下，按分數排序，同時返回value 和 score
        Set<ZSetOperations.TypedTuple<String>> rangeSet = zsetOps.rangeWithScores(1, 6);
        logger.info("在下標區間下，按分數排序，同時返回value 和 score:{}", rangeSet);


        //在分數區間下，按分數排序 同時返回value和score
        Set<ZSetOperations.TypedTuple<String>> scoreSet = zsetOps.rangeByScoreWithScores(0.1, 0.6);
        logger.info("在分數區間下，按分數排序 同時返回value和score:{}", scoreSet);


        //按從大到小排序
        Set<String> reverseSet = zsetOps.reverseRange(2, 8);
        logger.info("從大到小排序:{}", reverseSet);
        Map<String, Object> map = new HashMap<>();
        map.put("success", true);
        return map;
    }

    //測試Redisson
    @GetMapping("/redisson/{object}")
    public Map<String, Object> handleRedisson(@PathVariable String object) {
        Map<String, Object> returnMap = new HashMap<>();
        try {
            handleObject(object);
            returnMap.put("測試完成", object);
        } catch (Exception e) {
            logger.error("Redisson發生錯誤:{}", e.getMessage());
            returnMap.put("測試失敗", "Redisson");
        }
        return returnMap;
    }

    //測試處理Redisson 處理分配各private方法
    private void handleObject(String object) throws InterruptedException {
        switch (object) {
            case "rList":
                handleRList();
                break;
            case "rMap":
                handleRMap();
                break;
            case "rLock":
                handleRLock();
                break;
            case "atomic":
                handleAtomic();
                break;
        }
    }

    //測試Redisson  rList
    private void handleRList() {
        RedissonClient redissonClient = openRedissonClient();
        //RList 繼承了 java.util.List 介面
        RList<String> nameList = redissonClient.getList("nameList");
        nameList.clear();
        nameList.add("bingo");
        nameList.add("LiaoSmallYang");
        nameList.add("https://github.com/yanglbme");
        nameList.remove(-1);

        boolean contains = nameList.contains("LiaoSmallYang");
        logger.info("List size: " + nameList.size());
        logger.info("Is list contains name 'LiaoSmallYang'?:{}", contains);
        logger.info("redissonClient:{}", redissonClient);
        nameList.forEach(System.out::println);
        closeRedissonClient(redissonClient);
    }

    private void handleRMap() {
        RedissonClient redissonClient = openRedissonClient();
        //RMap繼承了java.util.concurrent.concurrentMap 介面
        RMap<String, String> map = redissonClient.getMap("personalInfo");
        map.put("name", "廖小羊");
        map.put("address", "101大樓");
        map.put("link", "https://google.com");

        boolean contains = map.containsKey("link");
        logger.info("Map size:{}", map.size());
        logger.info("Is map contains key 'link'? :{}", contains);

        String value = map.get("name");
        logger.info("value mapped by key 'name': " + value);
        boolean added = map.putIfAbsent("link", "https://google.com") == null;
        logger.info("value mapped by key 'link': " + added);

        closeRedissonClient(redissonClient);
    }

    private void handleRLock() throws InterruptedException {
        RedissonClient redissonClient = openRedissonClient();

        //RLock繼承 java.util.concurrent.locks.Lock 介面
        RLock rLock = redissonClient.getLock("rLock");
        rLock.lock();
        logger.info("lock acquired");

        Thread t = new Thread(() -> {
            RLock rLock1 = redissonClient.getLock("rLock");
            rLock1.lock();
            logger.info("lock acquired by thread");
            rLock1.unlock();
            logger.info("lock released by thread");
        });
        t.start();
        t.join(1000);

        rLock.unlock();
        logger.info("rLock released");
        t.join();

        closeRedissonClient(redissonClient);

    }

    //RAtomicLong操作
    private void handleAtomic(){
        RedissonClient redissonClient = openRedissonClient();

        RAtomicLong atomicLong = redissonClient.getAtomicLong("myLong");
        logger.info("Init value:{}", atomicLong.get());

        atomicLong.incrementAndGet();
        logger.info("Current value:{}", atomicLong.get());

        atomicLong.addAndGet(100L);

        logger.info("Final value:{}", atomicLong.get());

        closeRedissonClient(redissonClient);

    }

    //預防要在開啟啥東西或設定啥東西  先抽出來
    private RedissonClient openRedissonClient() {
        //預設連接上127.0.0.1:6379
        RedissonClient redissonClient = Redisson.create();
        return redissonClient;
    }

    private void closeRedissonClient(RedissonClient redissonClient) {
        redissonClient.shutdown();
    }

}

