package com.example.demo.config;


//建立Redis的Configuration
//Redis保存資料的方式是以key-value,所以config配置中透過KeyGenerator設定key值
//以及可以透過CacheManager來設定cache資料的預期時間

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;

import java.lang.reflect.Method;
import java.time.Duration;

@Configuration
@EnableCaching
public class RedisConfig {
    //redisConnectionFactory連線工廠鍵入cache監控
    @Bean(name = "redisConnectionFactory")
    public JedisConnectionFactory redisConnectionFactory() {
        return new JedisConnectionFactory();
    }

    // key值命名 輸入wiselyKeyGenerator 產生key
    @Bean
    public KeyGenerator wiselyKeyGenerator() {
        return new KeyGenerator() {
            @Override
            public Object generate(Object target, Method method, Object... params) {
                StringBuilder sb = new StringBuilder();
                sb.append(target.getClass().getName());
                sb.append(method.getName());
                for (Object obj : params) {
                    sb.append(obj.toString());
                }

                return sb.toString();
            }
        };
    }

    @Bean
    public RedisTemplate<String, String> redisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, String> redisTemplate = new RedisTemplate<String, String>();
        //建立redisTemplate模板，設定連線資訊，其他資訊設在application.properties
        //RedisConnectionFactory有設定cacheManager資訊內容，使用GenericJackson2JsonRedisSerializer序列化方法並設定緩存1小時
        redisTemplate.setConnectionFactory(factory);
        return redisTemplate;
    }

    @Bean
    public CacheManager cacheManager(RedisConnectionFactory factory) {
        //Redis序列化器
        //Redis 預設是用JdkSerializationRedisSerializer，用這個序列化的話被序列化的物件要實作Serializable介面
        //儲存內容時，除了屬性的內容外還存了其他內容在裡面，長度常不容易閱讀。
        //JacksonJsonRedisSerializer 與 GenericJackson2JsonRedisSerializer 兩個都能序列化成Json，後者在json中加入@class屬性 類別全路徑package名稱，方便反序列化
        //JacksonJsonRedisSerializer如果存放List 反序列化沒指定TypeRefference會噴錯java.util.LinkedHashMap cannot be cast to

        RedisSerializationContext.SerializationPair<Object> pair = RedisSerializationContext.SerializationPair
                .fromSerializer(new GenericJackson2JsonRedisSerializer());//用GenericJackson2JsonRedisSerializer序列化成Json
        RedisCacheConfiguration defaultCacheConfig = RedisCacheConfiguration.defaultCacheConfig()
                .serializeValuesWith(pair) // 序列化方式
                .entryTtl(Duration.ofHours(1)); // 過期時間

        return RedisCacheManager.builder(RedisCacheWriter.nonLockingRedisCacheWriter(factory))
                .cacheDefaults(defaultCacheConfig).build();

    }
}