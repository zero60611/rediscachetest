package com.example.demo.service.impl;

import com.example.demo.domain.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

//靈活的運用@Cacheable@CacheEvict@CachePut@Caching@CacheConfig
//達到目的地

@Service
@CacheConfig(cacheNames = "userService")
@Transactional
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    //建構子引入Ioc裡的東西
    public UserServiceImpl(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    /**
     * cacheNames 與 value 定義一样，若設置了 value 的值，cacheNames 配置就無效。
     * 使用 keyGenerator ，注意是否在config中定義好。
     */
    @Override
    //sync = true  執行同步，先將執行緒 緩存鎖住，等執行緒結束跑回結果更新到緩存中，再讓其他執行緒依序進入
    @Cacheable(value = "getAllUsers", keyGenerator = "wiselyKeyGenerator",sync = true)
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    /**
     * 執行時,將清除value = getAllUsers cache
     * 【cacheNames = "userService"】
     * 也可指定清除的key 【@CacheEvict(value="abc")】
     */
    @Override
    @CacheEvict(value = "getAllUsers",allEntries=true)
    public void clearAllUserCache() {}

    /**
     * key ="#p0" 表示已第一個參數作為key
     */
    @Override
    @Cacheable(value="redis", key ="#p0", condition = "#pId == 1")
    public User findById(Integer pId) {
        Optional<User> _User = userRepository.findById(pId);
        System.out.println("從資料庫查找完.....");
        return Optional.ofNullable(_User).get().orElse(null);
    }

    @Override
    @CacheEvict(value="user", key ="#p0")
    public void clear(Integer pId) {}
}
