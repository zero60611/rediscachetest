package com.example.demo.service;

import com.example.demo.domain.User;

import java.util.List;

public interface UserService {
    List<User> getAllUsers();

    User findById(Integer id);

    void clearAllUserCache();

    void clear(Integer pId);

}